module github.com/tidepool-org/clinic

go 1.13

require (
	github.com/deepmap/oapi-codegen v1.3.7
	github.com/getkin/kin-openapi v0.3.1
	github.com/labstack/echo/v4 v4.1.16
	go.mongodb.org/mongo-driver v1.3.2
	golang.org/x/crypto v0.0.0-20200406173513-056763e48d71 // indirect
	golang.org/x/net v0.0.0-20200324143707-d3edc9973b7e
	golang.org/x/sys v0.0.0-20200413165638-669c56c373c4 // indirect
)
