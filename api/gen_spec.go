// Package Clinic provides primitives to interact the openapi HTTP API.
//
// Code generated by github.com/deepmap/oapi-codegen DO NOT EDIT.
package api

import (
	"bytes"
	"compress/gzip"
	"encoding/base64"
	"fmt"
	"github.com/getkin/kin-openapi/openapi3"
	"strings"
)

// Base64 encoded, gzipped, json marshaled Swagger object
var swaggerSpec = []string{

	"H4sIAAAAAAAC/+xYS3PbNhD+Kxi0R8ZSmuiim+M2Gbd1rHE704PH04HJlYmUBGAA9KMK/3sHAB8gCUqU",
	"bKWXHDzG4LHY/Xa//SBucMxzwRkwrfByg1WcQk7s8CyjjMZmRLLsco2X1xssJBcgNQW742+amH/6WQBe",
	"YqUlZXe4LCMs4b6gEhK8vLabbqJ6E7/9ArHGZbTBP0pY4yX+YdZ6MKuun32Gx+r60pylOjOH66moGlHC",
	"ViBzqhTlzHqUgIolFZpyhpfYLZo1tOYSSciIWVEpFegW9CMAQ86SQoQlqLGqcNQLVXTvoRpyN0+0Bmku",
	"O01yyr6upPXgFuTXxhqO+hA1E0RK8owjXDB6X8C5s6plAQbFbti9YIeIVvuUF8WW3MV213kogVG1SAk7",
	"n5Bgf/PeiQ7GZnLezeTVXqnrAucD0qK0IprWRX8QRjQ8LZzdMeD2Q6dycjs2NQx1RMgHa4hGE3gZ4ZZm",
	"A+5cWg+Rm7yl7A4R5PAYcIMkiQRlh8CK3NSEBqW9YmjhyXhM3A0B7HLQJCGaeIttdTOSg1nIKfsd2J1O",
	"8fJt4AKRcgafi/wWZI+pHZ+Z3RH0wk1MSV6XxT5lW2D7pyL89AaeSC4y50g17uCI383nc3RBKEOJpA+A",
	"XbXhxfv3C+xjiP8iGSs0OpMA/+AOgA1i+FeeMnRRUImH6NQw4MVi8ab6W7Q+54QyXJYmskApjvTbtuE6",
	"COrC7NfltAa7vXP6mAc8HKTM7Kdsza3l6lxT1Q8glQvk7cncXMUFMCKoScjJ/GSOLbtT69bMnbLjO9BD",
	"LK5ASwoPgDKqNOLrijzqBKEr1zwVIkYyEBECkUKnXNJ/XV7tzdKOTR/Bn0BX5LUuSJKDtgm83mBqLrsv",
	"QBoZqTLO12sFptRcH/FgpEzDHUiDW/hoRnN62EnFpb6UCcjQ6YZEN0Y7lOBMubT/NJ/bTsuZBmZhJEJk",
	"1BX47ItynaK119TFblEJFEvZ752Xv9kSUkWeE/nchdo0E64CqT1NEkQQg8e2IXbzteLKS5jRSlD6A0+e",
	"9wp14vuodHo8xHRHpKdJ0piImnqebSo5T0pnIwMNQ2s/23l0Fo7frXbVN8EHeembMhkJcu0T6DFX2nzu",
	"8ONV0tLmZHqhuUfDkNOm1bTsin3v67eXeSjuIJsgOk6HiF3whK6fx0BbmUMh2I5byFHHzlOedc0EWvmW",
	"oFCr0geUnTO1nR+zuPPQ3q4CpKcD5pTVSDLWRIaV2/158l0GprGx9/w/XBGMhY+S59+EtluEpykgpPkE",
	"BQpWzzGoHID7xdpECfuTT6ZhM0vYDv26gpwbVirFY2qDRY9UpxWc9hdl7P18nyBvbdhnrQ8v1zxKWLfm",
	"RgXQL4zmt8b0lrLb6+MVyV4cPCLxom2mGmQOUd/d+jqeiWMxtf/R5VCy+lrpZWmErcL77jJay7VW1psb",
	"okLi83RCgTdfO74r5nTF9D8RHaKX9fmPXP6/YlnVz35C6ZXMEWWyxfgFIlkZ2SmRNY9mm2q0Qx7dfAPf",
	"WvJ8DEBfrPoIrurLJutghN+Hlj6QxH41AWUf9IvQnnOmQTKSoT9APoBEv0jJZVBZK7cm6+oUGMY7zy4Q",
	"jlVUexD1m7xqw+IqOui82g/baUkL6XA4ba/fBkKf9l9DgSu7rgKUpYLLYyEzvMSp1mI5m2U8JlnKlV6+",
	"m8/nuLwp/wsAAP//wbLWrQkcAAA=",
}

// GetSwagger returns the Swagger specification corresponding to the generated code
// in this file.
func GetSwagger() (*openapi3.Swagger, error) {
	zipped, err := base64.StdEncoding.DecodeString(strings.Join(swaggerSpec, ""))
	if err != nil {
		return nil, fmt.Errorf("error base64 decoding spec: %s", err)
	}
	zr, err := gzip.NewReader(bytes.NewReader(zipped))
	if err != nil {
		return nil, fmt.Errorf("error decompressing spec: %s", err)
	}
	var buf bytes.Buffer
	_, err = buf.ReadFrom(zr)
	if err != nil {
		return nil, fmt.Errorf("error decompressing spec: %s", err)
	}

	swagger, err := openapi3.NewSwaggerLoader().LoadSwaggerFromData(buf.Bytes())
	if err != nil {
		return nil, fmt.Errorf("error loading Swagger: %s", err)
	}
	return swagger, nil
}

