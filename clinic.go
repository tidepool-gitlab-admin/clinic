package main

import (
	"github.com/tidepool-org/clinic/api"
)

func main() {
    api.MainLoop()
}
